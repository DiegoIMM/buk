class SearchElements:
    def __init__(self, browser):
        """
        *Constructor of class*

        - `@param browser: The internet browser driver used in appium`
        """
        self.__browser = browser

    @staticmethod
    def input_search(context):
        return context.browser.find_element_by_xpath('//input[@name="keyword"]')

    @staticmethod
    def button_search(context):
        return context.browser.find_element_by_xpath('//input[@name="searchProducts"]')

    @staticmethod
    def table_results(context, term):
        return context.browser.find_element_by_xpath('//td[contains(text(),"' + term + '")]')
