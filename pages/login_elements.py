import os


class LoginElements:
    def __init__(self, browser):
        """
        *Constructor of class*

        - `@param browser: The internet browser driver used in appium`
        """
        self.__browser = browser

    @staticmethod
    def input_login(context, name):
        return context.browser.find_element_by_xpath('//input[@name="' + name + '"]')

    @staticmethod
    def select_pet(context):
        return context.browser.find_element_by_xpath('//select[@name="account.favouriteCategoryId"]')

    @staticmethod
    def options_pets(context, pet):
        return context.browser.find_element_by_xpath('//option[@value="' + pet + '"]')

    @staticmethod
    def button_create_account(context):
        return context.browser.find_element_by_xpath('//input[@name="newAccount"]')

    @staticmethod
    def input_sign_in_username(context):
        return context.browser.find_element_by_xpath('//input[@name="username"]')

    @staticmethod
    def input_sign_in_password(context):
        return context.browser.find_element_by_xpath('//input[@name="password"]')

    @staticmethod
    def button_sign_in(context):
        return context.browser.find_element_by_xpath('//input[@name="signon"]')
