import os


class BuyPetsElements:
    def __init__(self, browser):
        """
        *Constructor of class*

        - `@param browser: The internet browser driver used in appium`
        """
        self.__browser = browser

    @staticmethod
    def input_login(context, name):
        return context.browser.find_element_by_xpath('//input[@name="' + name + '"]')

    @staticmethod
    def product_id(context, id):
        return context.browser.find_element_by_xpath('//a[text()="' + id + '"]')

    @staticmethod
    def item_id(context, id):
        return context.browser.find_element_by_xpath('//a[text()="' + id + '"]')

    @staticmethod
    def add_to_cart(context):
        return context.browser.find_element_by_xpath('//a[text()="Add to Cart"]')

    @staticmethod
    def input_stock(context, id):
        return context.browser.find_element_by_xpath('//input[@name="' + id + '"]')

    @staticmethod
    def button_proceed_to_checkout(context):
        return context.browser.find_element_by_xpath('//a[text()="Proceed to Checkout"]')

    @staticmethod
    def button_continue_pay(context):
        return context.browser.find_element_by_xpath('//input[@value="Continue"]')

    @staticmethod
    def button_confirm_pay(context):
        return context.browser.find_element_by_xpath('//a[text()="Confirm"]')

    @staticmethod
    def input_payments_detail(context, name):
        return context.browser.find_element_by_xpath('//input[@name="' + name + '"]')

    @staticmethod
    def generic_table(context):
        return context.browser.find_element_by_xpath('//tbody')

    @staticmethod
    def validation_text(context):
        return context.browser.find_element_by_xpath('//li[text()="Thank you, your order has been submitted."]')

    @staticmethod
    def price_pet(context, price):
        return context.browser.find_element_by_xpath('//td[text()="$' + price + '"]')
