import os


class OrderElements:
    def __init__(self, browser):
        """
        *Constructor of class*

        - `@param browser: The internet browser driver used in appium`
        """
        self.__browser = browser

    @staticmethod
    def table_order_id(context, id):
        return context.browser.find_element_by_xpath('//a[text()="' + id + '"]')
