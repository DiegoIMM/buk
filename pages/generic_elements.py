import os


class GenericElements:
    def __init__(self, browser):
        """
        *Constructor of class*

        - `@param browser: The internet browser driver used in appium`
        """
        self.__browser = browser

    @staticmethod
    def menu_pets(context, index):
        return context.browser.find_element_by_xpath('//div[@id="QuickLinks"]//a[' + index + ']')

    @staticmethod
    def btn_sign_in(context):
        return context.browser.find_element_by_xpath('//div[@id="MenuContent"]//a[2]')

    @staticmethod
    def btn_my_account(context):
        return context.browser.find_element_by_xpath('//div[@id="MenuContent"]//a[3]')

    @staticmethod
    def btn_register_now(context):
        return context.browser.find_element_by_xpath('//div[@id="Catalog"]/a')

    @staticmethod
    def label_username(context):
        return context.browser.find_element_by_id('WelcomeContent')

    @staticmethod
    def button_my_orders(context):
        return context.browser.find_element_by_xpath('//a[text()="My Orders"]')
