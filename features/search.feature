@pruebasFeature
Feature: Buscar - Buscar una mascota

  @test @comprarMascota @formularioComprarMascota
  Scenario Outline: Validar que se pueda buscar una mascota
    Given Ir al sitio "https://petstore.octoperf.com/actions/Catalog.action"
    When Ingreso en el campo de busqueda el valor <buscar>
    And Presiono el boton Buscar
    Then Valido que se muestren resultados de la busqueda <buscar>


    Examples:
      | buscar |
      | Gold   |

