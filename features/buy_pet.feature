@pruebasFeature
Feature: Comprar - Flujo comprar una mascota

  @test @comprarMascota @formularioComprarMascota
  Scenario Outline: Comprar una mascota (solo formulario)
    Given Ir al sitio "https://petstore.octoperf.com/actions/Catalog.action"
    When Navego al formulario para iniciar sesion
    And Ingreso el username <username> y la password <password>
#   El numero es la posicion del animal que deseo en el menu
    And Selecciono del menu superior el tipo de mascota 2
    And Seleciono la mascota <product_id>
    And Selecciono el item <item_id>
    And Selecciono la cantidad de stock de mascotas que quiero <item_id> <stock>
    And Procedo a la pantalla de pago
    And Ingreso el valor en el input del formulario de pago
      | valor               | to_input         |
      | 4545 2132 1232 1231 | order.creditCard |
      | 21/02               | order.expiryDate |
    And Confirmo los datos de la compra
    Then Veo el mensaje de confirmacion de la compra


    Examples:
      | username       | password | product_id | item_id | stock |
      | DiegoMartinez2 | Pass     | K9-RT-02   | EST-24  | 1     |

