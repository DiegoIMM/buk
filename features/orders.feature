@pruebasFeature
Feature: Perfil de usuario - orden de compras

  @test @orders @myAccount
  Scenario Outline: Validar que se pueda ver la orden de compra (se asume que los datos ya existen)
    Given Ir al sitio "https://petstore.octoperf.com/actions/Catalog.action"
    When Navego al formulario para iniciar sesion
    And Ingreso el username <username> y la password <password>
    And Ingreso a la seccion mi cuenta
    And Navego a la seccion mis ordenes
    Then Deberia ver el numero de orden <orden>

    Examples:
      | username | password | orden |
      | juan20   | Pass     | 97493 |
