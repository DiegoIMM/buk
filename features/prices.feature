@precios
Feature: Validar precios de los productos

  @test @comprarMascota @formularioComprarMascota
  Scenario Outline: Se valida precio de los siguientes productos
    Given Ir al sitio "https://petstore.octoperf.com/actions/Catalog.action"
    When Selecciono del menu superior el tipo de mascota <tipo>
    And Seleciono la mascota <product_id>
    And Selecciono el item <item_id>
    Then Valido que el precio de la mascota sea <price>

    Examples:
      | product_id | item_id | price | tipo |
      | FI-SW-01   | EST-1   | 16.50 | 1    |
      | FI-SW-01   | EST-2   | 16.50 | 1    |
      | FI-SW-02   | EST-3   | 18.50 | 1    |
      | FI-FW-01   | EST-4   | 18.50 | 1    |
      | FI-FW-01   | EST-5   | 18.50 | 1    |
      | FI-FW-02   | EST-20  | 5.50  | 1    |
#      Este ultimo va a fallar aproposito
      | FI-FW-02   | EST-21  | 5.40  | 1    |
