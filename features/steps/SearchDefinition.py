import time

from behave import given, when, then, step
import os

from pages.search_elements import SearchElements


@when("Ingreso en el campo de busqueda el valor {buscar}")
def step_impl(context, buscar):
    SearchElements.input_search(context).send_keys(buscar)


@step("Presiono el boton Buscar")
def step_impl(context):
    SearchElements.button_search(context).click()


@then("Valido que se muestren resultados de la busqueda {buscar}")
def step_impl(context, buscar):
    assert SearchElements.table_results(context, buscar).is_displayed() is True, "No se encontraron resultados"
