import time

from behave import given, when, then, step
import os

from selenium.webdriver.support.select import Select

from pages.generic_elements import GenericElements
from pages.login_elements import LoginElements


@given(u'Ir al sitio "{url}"')
def ir_a_url_hakatools(context, url):
    print(url)
    context.browser.get(url)
    # context.window_control._go_to_url(url)


@when("Navego al formulario para crear cuenta")
def step_impl(context):
    GenericElements.btn_sign_in(context).click()
    time.sleep(2)
    GenericElements.btn_register_now(context).click()


@step("Ingreso el valor {valor} en el input {input}")
def step_impl(context, valor, input):
    raise NotImplementedError(u'STEP: And Ingreso el valor <valor> en el input <input>')


@step("Ingreso el valor en el input")
def step_impl(context):
    for row in context.table:
        LoginElements.input_login(context, row[1]).send_keys(row[0])


@step('Selecciono la mascota preferida {pet}')
def step_impl(context, pet):
    time.sleep(1)
    Select(LoginElements.select_pet(context)).select_by_index(1)


@step("Click en el boton Save Account")
def step_impl(context):
    LoginElements.button_create_account(context).click()


@step("Navego al formulario para iniciar sesion")
def step_impl(context):
    GenericElements.btn_sign_in(context).click()
    time.sleep(2)


@step("Ingreso el username {username} y la password {password}")
def step_impl(context, username, password):
    LoginElements.input_sign_in_username(context).clear()
    time.sleep(1)
    LoginElements.input_sign_in_username(context).send_keys(username)
    LoginElements.input_sign_in_password(context).clear()
    time.sleep(1)
    LoginElements.input_sign_in_password(context).send_keys(password)
    LoginElements.button_sign_in(context).click()
    time.sleep(5)


@then("Valido haber creado la cuenta")
def step_impl(context):
    assert GenericElements.label_username(context).is_displayed() is True, "No se logro guardar la cuenta"
