import time

from behave import given, when, then, step
import os

from selenium.webdriver.support.select import Select

from pages.generic_elements import GenericElements
from pages.login_elements import LoginElements
from pages.order_elements import OrderElements


@step("Ingreso a la seccion mi cuenta")
def step_impl(context):
    GenericElements.btn_my_account(context).click()


@step("Navego a la seccion mis ordenes")
def step_impl(context):
    GenericElements.button_my_orders(context).click()


@then("Deberia ver el numero de orden {orden}")
def step_impl(context, orden):
    assert OrderElements.table_order_id(context, orden).is_displayed() is True, "No se encontro la orden"
