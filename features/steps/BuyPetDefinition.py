import time

from behave import given, when, then, step

from pages.buy_pets_elements import BuyPetsElements
from pages.generic_elements import GenericElements


@step('Selecciono del menu superior el tipo de mascota {pet}')
def step_impl(context, pet):
    GenericElements.menu_pets(context, pet).click()


@step("Seleciono la mascota {product_id}")
def step_impl(context, product_id):
    BuyPetsElements.product_id(context, product_id).click()


@step("Selecciono el item {item_id}")
def step_impl(context, item_id):
    BuyPetsElements.item_id(context, item_id).click()
    time.sleep(2)
    BuyPetsElements.add_to_cart(context).click()


@step("Selecciono la cantidad de stock de mascotas que quiero {item_id} {stock}")
def step_impl(context, item_id, stock):
    BuyPetsElements.input_stock(context, item_id).send_keys(stock)


@step("Procedo a la pantalla de pago")
def step_impl(context):
    BuyPetsElements.button_proceed_to_checkout(context).click()


@step("Ingreso el valor en el input del formulario de pago")
def step_impl(context):
    for row in context.table:
        BuyPetsElements.input_payments_detail(context, row[1]).clear()
        BuyPetsElements.input_payments_detail(context, row[1]).send_keys(row[0])
    BuyPetsElements.button_continue_pay(context).click()


@step("Confirmo los datos de la compra")
def step_impl(context):
    assert BuyPetsElements.generic_table(context).is_displayed() is True, "No se muestra el resumen de la compra"
    BuyPetsElements.button_confirm_pay(context).click()


@then("Veo el mensaje de confirmacion de la compra")
def step_impl(context):
    assert BuyPetsElements.validation_text(context).is_displayed() is True, "No se muestra el mensaje de confirmacion"


@then("Valido que el precio de la mascota sea {price}")
def step_impl(context, price):
    assert BuyPetsElements.price_pet(context, price).is_displayed() is True, "No se muestra el precio de la mascota"
