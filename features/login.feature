@pruebasFeature
Feature: Login - Formulario para crear cuenta

  @test @login @registro
  Scenario Outline: Crear un usuario
    Given Ir al sitio "https://petstore.octoperf.com/actions/Catalog.action"
    When Navego al formulario para crear cuenta
    And Ingreso el valor en el input
      | valor             | to_input          |
      | DiegoMartinez2    | username          |
      | Pass              | password          |
      | Pass              | repeatedPassword  |
      | DiegoMartinez     | account.firstName |
      | Martinez          | account.lastName  |
      | email@example.cl  | account.email     |
      | 987336452         | account.phone     |
      | example address 1 | account.address1  |
      | example address 2 | account.address2  |
      | Chillan           | account.city      |
      | Nuble             | account.state     |
      | 2830000           | account.zip       |
      | Chile             | account.country   |
    And Selecciono la mascota preferida 1
    And Click en el boton Save Account
    And Navego al formulario para iniciar sesion
    And Ingreso el username <username> y la password <password>
    Then Valido haber creado la cuenta

    Examples:
      | username       | password |
      | DiegoMartinez2 | Pass     |






#
#  @alerts @selenium
#  Scenario Outline: Prueba de modal
#    Given Ir a la direccion de hakatools "https://www.hakalab.com/hakatools"
#    When Voy a modal
#    And Selecciono modal <tipo_modal>
#    And Realizo la prueba en modal <tipo_modal>
#    Then Valido que la prueba de modal <tipo_modal> se haya realizado correctamente
#
#    Examples:
#      | tipo_modal |
#      | pequeño    |
#      | grande     |