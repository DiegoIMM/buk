# Global python libraries
import os
from platform import system
import shutil
import pathlib
from selenium import webdriver

# Allure libraries
import allure
from allure_commons.types import AttachmentType
from dotenv import load_dotenv


def before_all(context):
    print("Before all")
    # If environment variables not exist, then load environment variables from file .env
    if os.getenv('BROWSER') is None:
        load_dotenv(dotenv_path='.env')
        assert os.getenv('BROWSER') is not None, "You must to define environment variables"


def after_all(context):
    print("After all")
    if os.getenv('EXECUTE_ALLURE') == "true":
        name_os = system()

        if name_os == 'Windows':
            os.system(
                str(pathlib.Path().absolute()) +
                '\\helper\\vendor\\allure-2.13.6\\bin\\allure.bat generate reporte -o report -c')

            if os.getenv('SAVE_REPORT_ALLURE') == "false":
                folder = str(pathlib.Path().absolute()) + '\\reporte'
                shutil.rmtree(folder, ignore_errors=True)


        elif name_os == 'Linux' or name_os == 'Darwin':
            os.system(
                str(pathlib.Path().absolute()) + '/helper/vendor/allure-2.13.6/bin/allure generate reporte -o report -c')


def before_scenario(context, scenario):
    print("Before scenario")
    execution_selenium(context)


def after_scenario(context, scenario):
    print("After scenario")
    attachment_browser_scenario_in_allure(context)
    # update_personal_report_allure(context)


def before_step(context, step):
    pass


def after_step(context, step):
    attachment_screenshot_step_in_allure(context)


def execution_selenium(context):
    name_os = system()
    try:
        if os.getenv('BROWSER') == "chrome":
            if name_os == "Windows":
                rute_driver = str(pathlib.Path().absolute(
                )) + "/helper/selenium_class/web_driver/" + os.getenv(
                    'BROWSER') + "/" + name_os + "/chromedriver.exe"
            elif name_os == "Linux" or name_os == "Darwin":
                rute_driver = str(pathlib.Path().absolute(
                )) + "/helper/selenium_class/web_driver/" + os.getenv('BROWSER') + "/" + name_os + "/chromedriver"

            context.browser = webdriver.Chrome(executable_path=rute_driver)

        context.browser.set_page_load_timeout(time_to_wait=200)
    except:
        assert False, "Connection webdriver is not correct, you should check connection rute"


def attachment_browser_scenario_in_allure(context):
    allure.attach(context.browser.name, name="Browser", attachment_type=AttachmentType.TEXT)


def attachment_screenshot_step_in_allure(context):
    allure.attach(context.browser.get_screenshot_as_png(), name="screenshot", attachment_type=AttachmentType.PNG)


def update_personal_report_allure(context):
    name_os = system()
    print(name_os)

    if name_os == 'Linux':
        file = open(str(pathlib.Path().absolute()) + "/reporte/environment.properties", "w+")
        file.write("Browser=" + context.browser.name.capitalize() + os.linesep)
        file.write("Browser.Version=Latest" + os.linesep)
        file.write("Stand=Develop")
        file.close()

    elif name_os == 'Windows':
        file = open(str(pathlib.Path().absolute()) +
                    "\\reporte\\environment.properties", "w+")
        file.write("Browser=" + context.browser.name.capitalize() + os.linesep)
        file.write("Browser.Version=Latest" + os.linesep)
        file.write("Stand=Develop")
        file.close()

    elif name_os == 'Darwin':
        file = open(str(pathlib.Path().absolute()) +
                    "/reporte/environment.properties", "w+")
        file.write("Browser=" + context.browser.name.capitalize() + os.linesep)
        file.write("Browser.Version=Latest" + os.linesep)
        file.write("Stand=Develop")
        file.close()
